﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mycompany.WebUI.Startup))]
namespace mycompany.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

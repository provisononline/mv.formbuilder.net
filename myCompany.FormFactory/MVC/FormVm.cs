﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace MyComany.FormFactory.MVC
{
    public class FormVm : Form, IHtmlString
    {
        private readonly IEnumerable<InputVm> inputs;

        public FormVm(string actionUri,
            string id,
            IEnumerable<InputVm> inputs = null) 
            : base(actionUri, id)
        {
            this.inputs = inputs;
        }

        private string Render()
        {
            var formTag = new TagBuilder("form");
            formTag.MergeAttribute("id", Id);

            if (inputs == null) return formTag.ToString();

            foreach (var input in inputs)
            {
                formTag.InnerHtml += input;
            }

            return formTag.ToString();
        }

        public override string ToString()
        {
            return Render();
        }

        public string ToHtmlString()
        {
            return ToString();
        }
    }
}

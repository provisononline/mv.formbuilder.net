﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace MyComany.FormFactory.MVC
{
    public class InputVm : Element, IHtmlString
    {
        public sealed override string Id { get; set; }
        public InputType InputType { get; set; }
        public Dictionary<string, string> HtmlAttributives { get; set; }
        public string Styles { get; set; }

        public InputVm(string id, InputType inputType)
        {
            Id = id;
            InputType = inputType;
        }

        public InputVm(string id,
            InputType inputType, 
            string styles, 
            Dictionary<string, string> htmlAttributives = null)
        {
            Id = id;
            InputType = inputType;
            Styles = styles;
            HtmlAttributives = htmlAttributives;
        }

        private string Render()
        {
            var tagBuilder = new TagBuilder("input");
            tagBuilder.MergeAttribute("name", Name);
            tagBuilder.MergeAttribute("id", Id);
            tagBuilder.MergeAttribute("type", InputType.ToString().ToLower());

            //TODO: HTML Name Parameters might be better placed
            if (Styles != null)
            {
                tagBuilder.MergeAttribute("class", Styles);
            }

            //Repeated Code??
            if (HtmlAttributives == null) return tagBuilder.ToString(TagRenderMode.SelfClosing);

            tagBuilder.MergeAttributes(HtmlAttributives);

            return tagBuilder.ToString(TagRenderMode.SelfClosing);
        }

        public string ToHtmlString()
        {
            return Render();
        }

        public override string ToString()
        {
            return ToHtmlString();
        }
    }
}

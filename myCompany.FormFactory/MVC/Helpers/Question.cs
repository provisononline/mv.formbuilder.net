﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace MyComany.FormFactory.MVC.Helpers
{
    public static class Question
    {
        public static InputVm Input(this HtmlHelper helper, string id)
        {
            return new InputVm(id, InputType.Text);
        }

        public static InputVm InputFor<TModel, TTextProperty>(this HtmlHelper<TModel> helper, string id, Expression<Func<TModel, TTextProperty>> expression)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            return new InputVm((string)metaData.Model, InputType.Text, null);
        }
        public static InputVm InputFor<TModel, TTextProperty>(this HtmlHelper<TModel> helper, string id, InputType type,  Expression<Func<TModel, TTextProperty>> expression)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            return new InputVm((string)metaData.Model, type, null);
        }
    }
}

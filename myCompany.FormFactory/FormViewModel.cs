﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using MyComany.FormFactory.Attributes;

namespace MyComany.FormFactory
{
    public class FormViewModel
    {
        //TODO: Would using Route Values be Better 
        public string ActionUri { get; set; }
        public string Method { get; set; }
        public string EncType { get; set; } = "multipart/form-data";

        public FormViewModel()
        {

        }

        public FormViewModel(MethodInfo methodInfo, string actionUri)
        {
            //TODO: Additional Check Reg Expression for when the string is not null or empty
            if (string.IsNullOrEmpty(actionUri))
            {
                throw new ArgumentException("Action Uri must has a value of /Controller/Action");

                //if (Regex.IsMatch(actionUri, ""))
                //{
                    
                //}
            }

            //var inputs = new List<PropertyVm>();


            foreach (var propInfo in methodInfo.GetParameters())
            {
                if (propInfo.GetCustomAttributes(true).Any(x => x is FormModelAttribute))
                {
                    //inputs.AddRange(pi.ParameterType.GetTypeInfo().DeclaredProperties
                                        //.Select(pi2 => new PropertyVm(pi, pi2).Then(p => p.Name = pi.Name + "." + p.Name)));
                }
                else
                {
                    //inputs.Add(new PropertyVm(pi));
                }
            }
        }
    }
}

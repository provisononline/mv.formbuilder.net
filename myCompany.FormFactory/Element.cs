﻿using System.Reflection;

namespace MyComany.FormFactory
{
    public abstract class Element
    {
        public abstract string Id { get; set; }

        public virtual string Name => !string.IsNullOrEmpty(Id) ? Id : string.Empty;
    }

    public class Input : Element
    {
        public sealed override string Id { get; set; }

        public Input(string id)
        {
            Id = id;
        }
    }

    public class Form : Element
    {
        public sealed override string Id { get; set; }
        private readonly string ActionUri;
        public string Method { get; set; }
        public string EncType { get; set; } = "multipart/form-data";

        public Form(string actionUri, string id)
        {
            ActionUri = actionUri;
            Id = id;
        }
    }

    public class Select : Element
    {
        public override string Id { get; set; }
    }

    public class TextBox : Input
    {
        public TextBox(string id) : base(id) { }
    }
}

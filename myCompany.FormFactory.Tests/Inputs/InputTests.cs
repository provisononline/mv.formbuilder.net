﻿using System;
using System.Web.Mvc;
using MyComany.FormFactory.MVC;
using NUnit.Framework;

namespace MyComany.FormFactory.Tests.Inputs
{
    [TestFixture]
    public class InputTests
    {
        [Test]
        public void ShouldRenderInputElement()
        {
            //Arrange
            const string name = "somename";

            var expected = string.Format(@"<input id=""{0}"" name=""{0}"" type=""{1}"" />", name,InputType.Text.ToString().ToLower());

            //Act
            var sut = new InputVm("somename", InputType.Text);
           
            //Assert
            Console.WriteLine(sut);
            Assert.That(sut.ToHtmlString(), Is.EqualTo(expected));
        }
        [Test]
        public void ShouldRenderInputElementWithStyles()
        {
            //Arrange
            var name = "somename";
            var styles = "form-control";

            var expected = string.Format(@"<input class=""{2}"" id=""{0}"" name=""{0}"" type=""{1}"" />", name,
                InputType.Text.ToString().ToLower(), styles);

            //Act
            var sut = new InputVm("somename", InputType.Text, styles);

            //Assert
            Console.WriteLine(sut);
            Assert.That(sut.ToHtmlString(), Is.EqualTo(expected));
        }

        [Test]
        public void ShouldRenderInputBuilder()
        {
            //Arrange
            var name = "somename";

            var expected = name;

            //Act
            var sut = new InputBuilder().WithId(name);

            //Assert
            Console.WriteLine(sut);
            Assert.That(sut.InputElement.Name, Is.EqualTo(expected));
        }



    }

    public class TestViewModel
    {
        public string Name { get; set; }
    }
}

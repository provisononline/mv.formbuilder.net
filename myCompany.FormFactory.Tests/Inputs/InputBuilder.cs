﻿using System.Web.Mvc;
using MyComany.FormFactory.MVC;

namespace MyComany.FormFactory.Tests.Inputs
{
    public class InputBuilder
    {
        internal InputBuilder WithId(string id)
        {
            InputElement = new InputVm(id, InputType.Text) {Id = id};
            return this;    
        }

        internal InputBuilder WithType(InputType type)
        {
            InputElement = new InputVm("someid", type);
            return this;
        }

        internal InputBuilder WithModel<T>(T model)
        {
            return this;
        }

        public InputVm InputElement { get; private set; }

        public override string ToString()
        {
            return InputElement.ToHtmlString();
        }

        public class InputVMTests
        {
            private Input input;

        }

    }
}

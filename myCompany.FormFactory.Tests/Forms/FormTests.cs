﻿using System;
using System.Collections.Generic;
using MyComany.FormFactory.MVC;
using MyComany.FormFactory.Tests.Inputs;
using NUnit.Framework;

namespace MyComany.FormFactory.Tests.Forms
{
    [TestFixture]
    public class FormTests
    {
        [Test]
        public void ShouldRenderForm()
        {
            //Will be factorty
         
            var form = new FormVm("Action", "id", null);
            var expected = @"<form id=""id""></form>";

            //Acts
            var sut = form.ToString();

            //Assert
            Assert.That(sut, Is.EqualTo(expected));
            Console.WriteLine(sut);
        }

        [Test]
        public void ShouldRenderFormWithInputs()
        {
            //Will be factorty
            var inputs = new List<InputVm>()
            {
                new InputBuilder().WithId("1").InputElement
            };

            var form = new FormVm("Action", "id", inputs);
            var expected = @"<form id=""id""></form>";

            //Acts
            var sut = form.ToString();

            //Assert
            Assert.That(sut, Is.EqualTo(expected));
            Console.WriteLine(sut);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Web;

namespace MyComany.FormFactory.Tests
{
    public class FakeHttpContext : HttpContextBase
    {
        private readonly Dictionary<object, object> items = new Dictionary<object, object>();

        public override IDictionary Items => items;
    }
}
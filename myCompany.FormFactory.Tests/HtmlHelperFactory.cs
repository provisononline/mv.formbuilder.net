﻿using System.Web.Mvc;

namespace MyComany.FormFactory.Tests
{
    public class HtmlHelperFactory
    {
        public static HtmlHelper Create()
        {
            var viewContent = new ViewContext {HttpContext = new FakeHttpContext()};
            var html = new HtmlHelper(viewContent, new FakeViewDataContainer());
            return html;
        }
    }
}

using System.Web.Mvc;

namespace MyComany.FormFactory.Tests
{
    public class FakeViewDataContainer : IViewDataContainer
    {
        public ViewDataDictionary ViewData { get; set; } = new ViewDataDictionary();
    }
}